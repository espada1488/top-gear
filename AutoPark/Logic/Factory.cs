﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace Logic
{
    public static class Factory
    {

         public static Park Create()
        {
            Park _cars = new Park();

            _cars.Add( new Car( 75, 2.2f, "Lada", 12500, 300000, 12.5f, "Red", 2004) );
            _cars.Add( new Bus( 110, 3.4f, "Pazik", 3400000, 370000, 20f, "White", 1999, 23, 2) );
            _cars.Add( new ArmoredBus (220, 5.4f, "Rhino", 8000, 1200000, 22f, "Grey", 2007, 10, 1, 12.5f) );
            _cars.Add( new Truck(350, 6.0f, "Kamaz", 50000, 600000, 25f, "Orange", 2005, 30, 6) );

            return _cars;
        }

      

        }
    }

