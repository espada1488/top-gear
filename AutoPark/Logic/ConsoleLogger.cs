﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic
{
   public class ConsoleLogger: ILogger
    {
       public void Log(String message)
       {
           Console.Write(message);
       }
    }
}
