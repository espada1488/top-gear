﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace Logic
{
    public class ParkCalculator
    {
        public long GetCost(Park park)
        {
            long Sum = 0;
           
            foreach (var car in park.Cars)
            {
                Sum += car.Cost;
            }
            return Sum;
        }
    }
}
