﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace Logic
{
    public class ParkPrinter
    {
        public void Print (Park park)
        {
            

            foreach (var car in park.Cars)
            {
                Console.Write("\n \n Engine Power= " + car.EnginePower + "\n Engine Capacity= " + car.EngineCapacity + "\n Make= " + car.Make + "\n Milage= " + car.Mileage + "\n Cost= " + car.Cost + "\n Consumption= " + car.Consumption + "\n Color= " + car.Color + "\n Year Of Release=" + car.YearOfRelease+ "\n "); 
                if(car is Bus){
                    Bus bus = (Bus)car;
                    Console.Write("Number of Seats= " + bus.NumberOfSeats + "\n Number of Doors= " + bus.NumberOfDoor+ "\n ");
                }
                if (car is ArmoredBus)
                {
                    ArmoredBus abus = (ArmoredBus)car;
                    Console.Write("Thickness= " +abus.Thickness);
                }
                if (car is Truck)
                {
                    Truck truck = (Truck)car;
                    Console.Write("Capacity= " + truck.Capacity + "\n Wheel amount= " + truck.WheelAmount + "\n");
                }
            }
            
        }
    }
}
