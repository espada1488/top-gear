﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic
{
  public  class LoggerFactory
    {
        public ILogger CreateLogger(int type)
        {
            switch (type)
            {
                case 1:
                    return new FileLogger();
                    
                case 2:
                    return new ConsoleLogger();
                    
                default:
                    throw new System.ArgumentException("Parameter must be 1 or 2", "type");
            }
        }
    }
}
