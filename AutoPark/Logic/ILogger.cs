﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic
{
   public interface ILogger
    {
        void Log(string message);
    }
}
