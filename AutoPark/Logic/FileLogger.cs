﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Logic
{
   public class FileLogger: ILogger
    {
        public void Log(String message)
        {
            using (StreamWriter sw = new StreamWriter( @"E:\\Log.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(message);
            }
        }
    }
}
