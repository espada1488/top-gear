﻿namespace AutoParkWindowsForm
{
    partial class AddForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.classes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.enginePower = new System.Windows.Forms.TextBox();
            this.engineCapacity = new System.Windows.Forms.TextBox();
            this.make = new System.Windows.Forms.TextBox();
            this.mileage = new System.Windows.Forms.TextBox();
            this.consumption = new System.Windows.Forms.TextBox();
            this.color = new System.Windows.Forms.TextBox();
            this.yearOfRelease = new System.Windows.Forms.TextBox();
            this.numberOfSeats = new System.Windows.Forms.TextBox();
            this.numberOfDoors = new System.Windows.Forms.TextBox();
            this.capacity = new System.Windows.Forms.TextBox();
            this.thickness = new System.Windows.Forms.TextBox();
            this.Add_form_accept = new System.Windows.Forms.Button();
            this.Add_form_cancel = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.cost = new System.Windows.Forms.TextBox();
            this.wheelAmount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // classes
            // 
            this.classes.FormattingEnabled = true;
            this.classes.Items.AddRange(new object[] {
            "Car",
            "Bus",
            "ArmoredBus",
            "Truck"});
            this.classes.Location = new System.Drawing.Point(151, 18);
            this.classes.Name = "classes";
            this.classes.Size = new System.Drawing.Size(100, 21);
            this.classes.TabIndex = 0;
            this.classes.SelectedIndexChanged += new System.EventHandler(this.classes_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Класс";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Мощность двигателя";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Объем двигателя";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Марка";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Пробег";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Расход топлива";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Цвет";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Год выпуска";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Число мест";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 296);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Число дверей";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 322);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Толщина брони";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Вместимость кузова";
            // 
            // enginePower
            // 
            this.enginePower.Location = new System.Drawing.Point(151, 73);
            this.enginePower.MaxLength = 4;
            this.enginePower.Name = "enginePower";
            this.enginePower.Size = new System.Drawing.Size(100, 20);
            this.enginePower.TabIndex = 13;
            // 
            // engineCapacity
            // 
            this.engineCapacity.Location = new System.Drawing.Point(151, 100);
            this.engineCapacity.MaxLength = 4;
            this.engineCapacity.Name = "engineCapacity";
            this.engineCapacity.Size = new System.Drawing.Size(100, 20);
            this.engineCapacity.TabIndex = 14;
            // 
            // make
            // 
            this.make.Location = new System.Drawing.Point(151, 127);
            this.make.MaxLength = 40;
            this.make.Name = "make";
            this.make.Size = new System.Drawing.Size(100, 20);
            this.make.TabIndex = 15;
            // 
            // mileage
            // 
            this.mileage.Location = new System.Drawing.Point(151, 154);
            this.mileage.MaxLength = 7;
            this.mileage.Name = "mileage";
            this.mileage.Size = new System.Drawing.Size(100, 20);
            this.mileage.TabIndex = 16;
            // 
            // consumption
            // 
            this.consumption.Location = new System.Drawing.Point(151, 181);
            this.consumption.MaxLength = 2;
            this.consumption.Name = "consumption";
            this.consumption.Size = new System.Drawing.Size(100, 20);
            this.consumption.TabIndex = 17;
            // 
            // color
            // 
            this.color.Location = new System.Drawing.Point(151, 208);
            this.color.MaxLength = 15;
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(100, 20);
            this.color.TabIndex = 18;
            // 
            // yearOfRelease
            // 
            this.yearOfRelease.Location = new System.Drawing.Point(151, 235);
            this.yearOfRelease.MaxLength = 4;
            this.yearOfRelease.Name = "yearOfRelease";
            this.yearOfRelease.Size = new System.Drawing.Size(100, 20);
            this.yearOfRelease.TabIndex = 19;
            // 
            // numberOfSeats
            // 
            this.numberOfSeats.Location = new System.Drawing.Point(151, 262);
            this.numberOfSeats.MaxLength = 3;
            this.numberOfSeats.Name = "numberOfSeats";
            this.numberOfSeats.Size = new System.Drawing.Size(100, 20);
            this.numberOfSeats.TabIndex = 20;
            // 
            // numberOfDoors
            // 
            this.numberOfDoors.Location = new System.Drawing.Point(151, 289);
            this.numberOfDoors.MaxLength = 2;
            this.numberOfDoors.Name = "numberOfDoors";
            this.numberOfDoors.Size = new System.Drawing.Size(100, 20);
            this.numberOfDoors.TabIndex = 21;
            // 
            // capacity
            // 
            this.capacity.Location = new System.Drawing.Point(151, 341);
            this.capacity.MaxLength = 5;
            this.capacity.Name = "capacity";
            this.capacity.Size = new System.Drawing.Size(100, 20);
            this.capacity.TabIndex = 22;
            // 
            // thickness
            // 
            this.thickness.Location = new System.Drawing.Point(151, 315);
            this.thickness.MaxLength = 4;
            this.thickness.Name = "thickness";
            this.thickness.Size = new System.Drawing.Size(100, 20);
            this.thickness.TabIndex = 23;
            // 
            // Add_form_accept
            // 
            this.Add_form_accept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Add_form_accept.Location = new System.Drawing.Point(16, 462);
            this.Add_form_accept.Name = "Add_form_accept";
            this.Add_form_accept.Size = new System.Drawing.Size(75, 23);
            this.Add_form_accept.TabIndex = 24;
            this.Add_form_accept.Text = "Ok";
            this.Add_form_accept.UseVisualStyleBackColor = true;
            this.Add_form_accept.Click += new System.EventHandler(this.Add_form_accept_Click);
            // 
            // Add_form_cancel
            // 
            this.Add_form_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Add_form_cancel.Location = new System.Drawing.Point(151, 462);
            this.Add_form_cancel.Name = "Add_form_cancel";
            this.Add_form_cancel.Size = new System.Drawing.Size(75, 23);
            this.Add_form_cancel.TabIndex = 25;
            this.Add_form_cancel.Text = "Cancel";
            this.Add_form_cancel.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Стоимость";
            // 
            // cost
            // 
            this.cost.Location = new System.Drawing.Point(151, 45);
            this.cost.MaxLength = 10;
            this.cost.Name = "cost";
            this.cost.Size = new System.Drawing.Size(100, 20);
            this.cost.TabIndex = 27;
            this.cost.Leave += new System.EventHandler(this.cost_Leave);
            // 
            // wheelAmount
            // 
            this.wheelAmount.Location = new System.Drawing.Point(151, 376);
            this.wheelAmount.MaxLength = 2;
            this.wheelAmount.Name = "wheelAmount";
            this.wheelAmount.Size = new System.Drawing.Size(100, 20);
            this.wheelAmount.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 382);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Число колес";
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 497);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.wheelAmount);
            this.Controls.Add(this.cost);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Add_form_cancel);
            this.Controls.Add(this.Add_form_accept);
            this.Controls.Add(this.thickness);
            this.Controls.Add(this.capacity);
            this.Controls.Add(this.numberOfDoors);
            this.Controls.Add(this.numberOfSeats);
            this.Controls.Add(this.yearOfRelease);
            this.Controls.Add(this.color);
            this.Controls.Add(this.consumption);
            this.Controls.Add(this.mileage);
            this.Controls.Add(this.make);
            this.Controls.Add(this.engineCapacity);
            this.Controls.Add(this.enginePower);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.classes);
            this.Name = "AddForm";
            this.Text = "AddForm";
            this.Load += new System.EventHandler(this.addForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox classes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox enginePower;
        private System.Windows.Forms.TextBox engineCapacity;
        private System.Windows.Forms.TextBox make;
        private System.Windows.Forms.TextBox mileage;
        private System.Windows.Forms.TextBox consumption;
        private System.Windows.Forms.TextBox color;
        private System.Windows.Forms.TextBox yearOfRelease;
        private System.Windows.Forms.TextBox numberOfSeats;
        private System.Windows.Forms.TextBox numberOfDoors;
        private System.Windows.Forms.TextBox capacity;
        private System.Windows.Forms.TextBox thickness;
        private System.Windows.Forms.Button Add_form_accept;
        private System.Windows.Forms.Button Add_form_cancel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox cost;
        private System.Windows.Forms.TextBox wheelAmount;
        private System.Windows.Forms.Label label13;

    }
}