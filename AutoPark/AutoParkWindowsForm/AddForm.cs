﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;

namespace AutoParkWindowsForm
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }

        public AddForm(Car car) 
        {
            InitializeComponent();
            cost.Text = car.Cost.ToString();
            enginePower.Text = car.EnginePower.ToString();
            engineCapacity.Text = car.EngineCapacity.ToString();
            make.Text = car.Make.ToString();
            mileage.Text = car.Mileage.ToString();
            consumption.Text = car.Consumption.ToString();
            color.Text = car.Color.ToString();
            yearOfRelease.Text = car.YearOfRelease.ToString();
            if (car is Bus)
            {
                Bus bus = (Bus)car;
                numberOfSeats.Text = bus.NumberOfSeats.ToString();
                numberOfDoors.Text = bus.NumberOfDoor.ToString();
            }
            if (car is ArmoredBus)
            {
                ArmoredBus abus = (ArmoredBus)car;
                thickness.Text = abus.Thickness.ToString();
            }
           if( car is Truck)
           {Truck truc = (Truck)car;
            capacity.Text = truc.Capacity.ToString();
            wheelAmount.Text = truc._wheelAmount.ToString();
           }
        }
        private Car _car;
        public Car Car
        {
            get { return _car; }
            set { _car = value; }
        }

        private void Add_form_accept_Click(object sender, EventArgs e)
        {
            switch (classes.Text)
            {
                case "Car":
                    _car = new Car(Int32.Parse(enginePower.Text), float.Parse(engineCapacity.Text), make.Text, Int32.Parse(mileage.Text), long.Parse(cost.Text), float.Parse(consumption.Text), color.Text, Int32.Parse(yearOfRelease.Text));
                    break;
                case "Bus":
                    _car = new Bus(Int32.Parse(enginePower.Text), float.Parse(engineCapacity.Text), make.Text, Int32.Parse(mileage.Text), long.Parse(cost.Text), float.Parse(consumption.Text), color.Text, Int32.Parse(yearOfRelease.Text), Int32.Parse(numberOfSeats.Text),Int32.Parse(numberOfDoors.Text));
                    break;
                case "ArmoredBus":
                    _car = new ArmoredBus(Int32.Parse(enginePower.Text), float.Parse(engineCapacity.Text), make.Text, Int32.Parse(mileage.Text), long.Parse(cost.Text), float.Parse(consumption.Text), color.Text, Int32.Parse(yearOfRelease.Text), Int32.Parse(numberOfSeats.Text), Int32.Parse(numberOfDoors.Text), float.Parse(thickness.Text));
                    break;
                case "Truck":
                    _car = new Truck(Int32.Parse(enginePower.Text), float.Parse(engineCapacity.Text), make.Text, Int32.Parse(mileage.Text), long.Parse(cost.Text), float.Parse(consumption.Text), color.Text, Int32.Parse(yearOfRelease.Text), double.Parse(capacity.Text), Int32.Parse(wheelAmount.Text));
                    break;
                default:
                    MessageBox.Show("Выбран несуществующий класс");
                    break;
            }
           
        
        }

    private void addForm_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(classes.SelectedText))
                {
                    Add_form_accept.Enabled = false;
                }
    }


    private void cost_Leave(object sender, EventArgs e)
    {
        int result;
        if (Int32.TryParse(cost.Text, out result))
        {
            return;
        }
        MessageBox.Show("неверный данны для стоимости");
        cost.Text = null;
    }

           private void classes_SelectedIndexChanged(object sender, EventArgs e)
            {
                    Add_form_accept.Enabled = true;
            }
    }
}
