﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Logic;

namespace AutoParkWindowsForm
{
    public partial class MainForm : Form
    {
        private static Park park = Factory.Create();
  

        ParkCalculator calculator = new ParkCalculator();
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            foreach (var car in park.Cars){
                listBox1.Items.Add( car.Make + " "+ car.Cost);
            }

            long Cost = calculator.GetCost(park);
            label2.Text = Cost.ToString();
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            if (  addForm.ShowDialog(this) == DialogResult.OK)
            {
                Car newCar = addForm.Car;
                listBox1.Items.Add(newCar.Make + " " + newCar.Cost);
                park.Add(newCar);

                long Cost = calculator.GetCost(park);

                label2.Text = Cost.ToString();
            }

        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count != 0){
                park.Delete(listBox1.Items.IndexOf(listBox1.SelectedItems[0] ));
            listBox1.Items.Remove(listBox1.SelectedItems[0]);
            long Cost = calculator.GetCost(park);
  
            label2.Text = Cost.ToString();
            }
        }

        private void Button_Edit_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count != 0)
            {
                int index = listBox1.Items.IndexOf(listBox1.SelectedItems[0]);
                AddForm addForm = new AddForm(park.GetElementByIndex(index));

                if (addForm.ShowDialog(this) == DialogResult.OK)
                {
                    Car newCar = addForm.Car;
                    listBox1.Items[index] = newCar.Make.ToString() + " " + newCar.Cost.ToString();
                    park.Edit(index,newCar);
                    long Cost = calculator.GetCost(park);

                    label2.Text = Cost.ToString();
                }
            
            }
        }
    }
}
