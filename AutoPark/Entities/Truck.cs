﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Truck : Car
    {
        public double _capacity;
        public int _wheelAmount;

        public Truck(int enginePower, float engineCapacity, String make, int mileage, long cost, float consumption, String color, int yearOfRelease, double capacity, int wheelAmount)
            : base(enginePower, engineCapacity, make, mileage, cost, consumption, color, yearOfRelease)
        {
            _wheelAmount = wheelAmount;
        }
        public double Capacity
        {
            get { return _capacity; }
            set { _capacity = value; }
        }
        public int WheelAmount
        {
            get { return _wheelAmount; }
            set { _wheelAmount = value; }
        }
    }
}
