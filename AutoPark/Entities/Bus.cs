﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Bus : Car
    {
        private int _numberOfSeats;
        private int _numberOfDoor;

        public Bus (int enginePower, float engineCapacity, String make, int mileage, long cost, float consumption, String color, int yearOfRelease, int numberOfSeats, int numberOfDoor) 
            :base( enginePower, engineCapacity, make, mileage, cost, consumption, color, yearOfRelease)
        {
            _numberOfSeats = numberOfSeats;
            _numberOfDoor = numberOfDoor;
        }

        public int NumberOfSeats
        {
            get { return _numberOfSeats;}
            set { _numberOfSeats = value; }
        }

        public int NumberOfDoor
        {
            get { return _numberOfDoor;}
            set { _numberOfDoor = value; }
        }
    }
}
