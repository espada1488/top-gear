﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Entities
{
    public class Park
    {
        private List<Car> _cars = new List<Car>();


        public List<Car> Cars
        {
            get { return _cars; }

        }

        public void Add(Car car)
        {
            _cars.Add(car);
        }
        public void Edit(int index, Car car)
        {
            _cars[index] = car;
        }
        public void Delete(int index)
        {
            _cars.RemoveAt(index);
        }
        public Car GetElementByIndex(int index)
        {
            return _cars[index] ;
        }

    }
}