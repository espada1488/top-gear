﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Car
    {
        private int _enginePower; 
        private float _engineCapacity; 
        private String _make; 
        private int _mileage; 
        private long _cost; 
        private float _consumption; 
        private String _color; 
        private int _yearOfRelease; 

        public Car(int enginePower, float engineCapacity, String make, int mileage, long cost, float consumption, String color, int yearOfRelease)
        {
            _enginePower = enginePower;
            _engineCapacity = engineCapacity;
            _make = make;
            _mileage = mileage;
            _cost = cost;
            _consumption = consumption;
            _color = color;
            _yearOfRelease = yearOfRelease;
        }

       public int EnginePower
        {
            get { return _enginePower; }
            set {  _enginePower=value; }
        }
       public float EngineCapacity
        {
            get { return _engineCapacity; }
            set { _engineCapacity = value; }
        }
       public String Make
        {
            get { return _make; }
            set { _make = value; }
        }
       public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }
       public long Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
       public float Consumption
        {
            get { return _consumption; }
            set { _consumption = value; }
        }
       public String Color
        {
            get { return _color; }
            set { _color = value; }
        }
       public int YearOfRelease
        {
            get { return _yearOfRelease;}
            set { _yearOfRelease = value; }
        }

    }
}
