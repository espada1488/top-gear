﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ArmoredBus : Bus
    {
        private float _thickness;
    

    public ArmoredBus (int enginePower, float engineCapacity, String make, int mileage, long cost, float consumption, String color, int yearOfRelease, int numberOfSeats, int numberOfDoor, float thickness) 
        :base ( enginePower,  engineCapacity, make, mileage, cost, consumption, color, yearOfRelease, numberOfSeats,  numberOfDoor)
    {
        _thickness = thickness;
    }

    public float Thickness
    {
        get { return _thickness;}
        set { _thickness = value;}
        }
    }

    }

