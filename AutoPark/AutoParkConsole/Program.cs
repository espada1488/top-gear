﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Logic;

namespace AutoParkConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            Park park = Factory.Create();


            ParkCalculator calculator = new ParkCalculator();

            ParkPrinter parkprinter = new ParkPrinter();
            parkprinter.Print(park);
            long Cost = calculator.GetCost(park);
            Console.Write("Total Cost " + Cost);
            Console.ReadKey();
        }
    }
}
